# frozen_string_literal: true

# Classe Primeiro Acesso
class Firstaccess
  attr_reader :link_first_access, :field_cpf, :field_date_of_birth, :radio_terms, :btn_check,
              :field_email, :field_phone, :btn_validate_email, :field_register_pass,
              :btn_show_pass, :msg_textinput_error, :btn_validate_pass, :mss_dialog,
              :leave_terms, :msg_success

  def initialize
    @field_cpf = EL_ANDROID['field_cpf']
    @set_input_cpf = CONFIG['register_first_access']['cpf']
    @set_input_cpf_invalid = CONFIG['register_first_access']['cpf_invalid']
    @field_date_of_birth = EL_ANDROID['field_date_of_birth']
    @input_date_of_birth = CONFIG['register_first_access']['date_of_birth']
    @set_input_date_invalid = CONFIG['register_first_access']['date_of_birth_invalid']
    @radio_terms = EL_ANDROID['radio_terms']
    @btn_check = EL_ANDROID['btn_check']
    @field_email = EL_ANDROID['field_email']
    @btn_validate_email = EL_ANDROID['btn_validate_email']
    @field_register_pass = EL_ANDROID['field_register_pass']
    @set_input_register_pass = CONFIG['register_first_access']['register_pass']
    @set_input_register_pass_invalid = CONFIG['register_first_access']['register_pass_invalid']
    @btn_show_pass = EL_ANDROID['btn_show_pass']
    @msg_textinput_error = EL_ANDROID['msg_textinput_error']
    @btn_validate_pass = EL_ANDROID['btn_validate_pass']
    @mss_dialog = EL_ANDROID['mss_dialog']
    @leave_terms = EL_ANDROID['leave_terms']
    @msg_success = EL_ANDROID['msg_success']
  end

  def first_access_success
    find_element(id: @field_cpf).send_keys(@set_input_cpf)
    find_element(id: @field_date_of_birth).send_keys(@input_date_of_birth)
    find_element(id: @radio_terms).click
    find_element(xpath: @leave_terms).click
    find_element(id: @btn_check).click
    find_element(id: @btn_validate_email).click
    find_element(id: field_register_pass).send_keys(@set_input_register_pass)
    find_element(id: @btn_show_pass).click
    find_element(id: @btn_validate_pass).click
  end

  def invalid_cpf_registration
    find_element(id: @field_cpf).send_keys(@set_input_cpf_invalid)
  end

  def invalid_date_of_birth
    find_element(id: @field_cpf).send_keys(@set_input_cpf)
    find_element(id: @field_date_of_birth).send_keys(@set_input_date_invalid)
    find_element(id: @radio_terms).click
    find_element(xpath: @leave_terms).click
    find_element(id: @btn_check).click
  end

  def message_invalid_user
    find_element(id: @mss_dialog).text
  end

  def message_invalid_cpf
    find_element(id: @msg_textinput_error).text
  end

  def invalid_password
    find_element(id: @field_cpf).send_keys(@set_input_cpf)
    find_element(id: @field_date_of_birth).send_keys(@input_date_of_birth)
    find_element(id: @radio_terms).click
    find_element(xpath: @leave_terms).click
    find_element(id: @btn_check).click
    find_element(id: @btn_validate_email).click
    find_element(id: field_register_pass).send_keys(@set_input_register_pass_invalid)
    find_element(id: @btn_show_pass).click
    find_element(id: @btn_validate_pass).click
  end

  def message_pass_invalid
    find_element(id: msg_textinput_error).text
  end

  def message_success
    find_element(id: @msg_success).text
  end
end
