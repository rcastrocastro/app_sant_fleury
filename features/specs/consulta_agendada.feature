#language: pt

@smoke
Funcionalidade: Agendar consulta online
  Como um usuário
  Quero poder agendar uma consulta
  Para que possa realizar a consulta no horário agendado

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Agendamento da consulta por data
  Dado que o paciente selecione a opcao de agendar consulta
  Quando escolher a data com o horario da consulta
  Então o sistema deverá apresentar na home a consulta agendada

Cenário: Agendamento da consulta por médico
  Dado que o paciente selecione a opcao de agendar consulta
  Quando escolher o medico
  Então o sistema deverá apresentar na home a consulta agendada