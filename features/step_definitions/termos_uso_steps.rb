Quando("acessar os termos") do
  @screen.profile.access_view_terms
end

Então("o sistema deverá redirecionar o paciente para os termos") do |expect_message|
  expect(@screen.terms.validate_page_terms).to eql expect_message
end