# frozen_string_literal: true

# Classe HOME iOS
class HomeIOS
  attr_reader :logged_user_ios, :access_profile_ios, :btn_consultations_ios,
              :displayed_consultation_ios, :btn_detail_consultation_ios

  def initialize
    @logged_user_ios = EL_IOS['logged_user_ios']
    @access_profile_ios = EL_IOS['access_profile_ios']
    @btn_consultations_ios = EL_IOS['btn_consultations_ios']
    @displayed_consultation_ios = EL_IOS['displayed_consultation_ios']
    @btn_detail_consultation_ios = EL_IOS['btn_detail_consultation_ios']
  end

  def user_logged
    sleep 5
    find_elements(xpath: @logged_user_ios)[0].text
  end

  def profile_access
    find_element(xpath: @access_profile_ios).click
  end

  def access_schedule
    find_element(xpath: @btn_consultations_ios).click
  end

  def validate_schedule
    find_element(xpath: @displayed_consultation_ios).displayed?
  end

  def select_query_detail
    find_elements(xpath: @btn_detail_consultation_ios).sample.click
  end
end
