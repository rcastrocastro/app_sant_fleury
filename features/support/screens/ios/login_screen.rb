# frozen_string_literal: true

# Classe Login
class LoginIOS
  attr_reader :btn_skip_ios, :user_cpf_ios_ios, :user_pass_ios_ios, :btn_entrar_ios, :msg_invalid_login_ios,
              :validate_page_login_ios, :link_first_access_ios, :close_keyboard_ios, :btn_allow_ios

  def initialize
    @user_app_ios = CONFIG['app']['user_app']
    @user_app_pass_ios = CONFIG['app']['user_app_pass']
    @btn_skip_ios = EL_IOS['btn_skip_ios']
    @user_cpf_ios = EL_IOS['user_cpf_ios']
    @user_pass_ios = EL_IOS['user_pass_ios']
    @btn_entrar_ios = EL_IOS['btn_entrar_ios']
    @msg_invalid_login_ios = EL_IOS['msg_invalid_login_ios']
    @validate_page_login_ios = EL_IOS['validate_page_login_ios']
    @link_first_access_ios = EL_IOS['link_first_access_ios']
    @close_keyboard_ios = EL_IOS['close_keyboard_ios']
    @btn_allow_ios = EL_IOS['btn_allow_ios']
  end

  def closed_alert
    find_element(xpath: @btn_allow_ios).click
  end

  def sign_in
    closed_alert
    find_element(xpath: @btn_skip_ios).click
    find_element(xpath: @user_cpf_ios).send_keys(@user_app_ios)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @user_pass_ios).send_keys(@user_app_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @btn_entrar_ios).click
  end

  def sign_in_failure(document, password)
    closed_alert
    find_element(xpath: @btn_skip_ios).click
    find_element(xpath: @user_cpf_ios).send_keys(document)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @user_pass_ios).send_keys(password)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @btn_entrar_ios).click
  end

  def message_get
    find_element(xpath: @msg_invalid_login_ios, wait: 5).text
  end

  def validate_element
    find_element(xpath: @validate_page_login_ios)
  end

  def access_first_registration
    closed_alert
    find_element(xpath: @btn_skip_ios).click
    find_element(xpath: @link_first_access_ios).click
  end
end
