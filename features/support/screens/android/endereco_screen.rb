# frozen_string_literal: true

# Classe Alterar Endereco
class Address
  attr_reader :input_street, :input_neighborhood, :input_city, :input_state, :input_zipcode,
              :clear_input_zipcode, :btn_refresh_address, :title_dialog

  def initialize
    @input_street = EL_ANDROID['input_street']
    @set_input_street = CONFIG['address']['street']
    @input_neighborhood = EL_ANDROID['input_neighborhood']
    @set_input_neighborhood = CONFIG['address']['neighborhood']
    @input_city = EL_ANDROID['input_city']
    @set_input_city = CONFIG['address']['city']
    @input_state = EL_ANDROID['input_state']
    @set_input_state = CONFIG['address']['state']
    @input_zipcode = EL_ANDROID['input_zipcode']
    @set_input_zipcode = CONFIG['address']['zipcode']
    @clear_input_zipcode = EL_ANDROID['clear_input_zipcode']
    @btn_refresh_address = EL_ANDROID['btn_refresh_address']
    @title_dialog = EL_ANDROID['title_dialog']
  end

  def update_address
    find_element(id: @input_street).clear
    find_element(id: @input_street).send_keys(@set_input_street)
    find_element(id: @input_neighborhood).clear
    find_element(id: @input_neighborhood).send_keys(@set_input_neighborhood)
    find_element(id: @input_city).clear
    find_element(id: @input_city).send_keys(@set_input_city)
    find_element(id: @input_state).clear
    find_element(id: @input_state).send_keys(@set_input_state)
    find_element(id: @clear_input_zipcode).click
    find_element(id: @input_zipcode).send_keys(@set_input_zipcode)
    find_element(id: @btn_refresh_address).click
  end

  def validate_message_success_update_address
    find_element(@title_dialog).text
  end
end
