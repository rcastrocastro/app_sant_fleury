Dado("que o paciente selecione a opcao de consulta online") do
  @screen.home.access_schedule
  @screen.consultation.access_consult_now
end

Quando("escolher a opcao de aguardar na fila") do
  @screen.schedule_now.access_queue
end

Então("o sistema deverá redirecionar o pacinte para a fila de atendimento") do
  expect(@screen.schedule_now.validate_queue.displayed?).to be true
end