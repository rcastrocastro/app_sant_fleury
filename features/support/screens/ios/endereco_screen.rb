# frozen_string_literal: true

# Classe Alterar Endereco
class AddressIOS
  attr_reader :edit_field_input_address_ios, :close_keyboard_ios, :btn_refresh_address_ios, :title_dialog_ios

  def initialize
    @edit_field_input_address_ios = EL_IOS['edit_field_input_address_ios']
    @set_input_street_ios = CONFIG['address']['street']
    @set_input_neighborhood_ios = CONFIG['address']['neighborhood']
    @set_input_city_ios = CONFIG['address']['city']
    @set_input_state_ios = CONFIG['address']['state']
    @set_input_zipcode_ios = CONFIG['address']['zipcode']
    @btn_refresh_address_ios = EL_IOS['btn_refresh_address_ios']
    @close_keyboard_ios = EL_IOS['close_keyboard_ios']
    @title_dialog_ios = EL_IOS['title_dialog_ios']
  end

  def update_address
    fields = find_elements(xpath: @edit_field_input_address_ios)
    fields[0].clear
    fields[0].send_keys(@set_input_street_ios)
    find_element(xpath: @close_keyboard_ios).click
    fields[1].clear
    fields[1].send_keys(@set_input_neighborhood_ios)
    find_element(xpath: @close_keyboard_ios).click
    fields[2].clear
    fields[2].send_keys(@set_input_city_ios)
    find_element(xpath: @close_keyboard_ios).click
    fields[3].clear
    fields[3].send_keys(@set_input_state_ios)
    find_element(xpath: @close_keyboard_ios).click
    fields[4].clear
    fields[4].send_keys(@set_input_zipcode_ios)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @btn_refresh_address_ios).click
  end

  def validate_message_success_update_address
    find_element(xpath: @title_dialog_ios).text
  end
end
