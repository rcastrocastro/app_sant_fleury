# frozen_string_literal: true

# Classe Escolher tipo da consulta
class ConsultationIOS
  attr_reader :btn_consult_now_ios, :btn_schedule_consult_ios

  def initialize
    @btn_consult_now_ios = EL_IOS['btn_consult_now_ios']
    @btn_schedule_consult_ios = EL_IOS['btn_schedule_consult_ios']
  end

  def access_consult_now
    find_element(xpath: @btn_consult_now_ios).click
  end

  def access_schedule_online
    find_element(xpath: @btn_schedule_consult_ios).click
  end
end
