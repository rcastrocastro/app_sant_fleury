#language: pt

@smoke
Funcionalidade: Realizar consulta online
  Como um usuário
  Quero poder entrar na fila de atendimento
  Para que possa realizar a consulta online

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Efetuar a consulta online com o médico
  Dado que o paciente selecione a opcao de consulta online
  Quando escolher a opcao de aguardar na fila
  Então o sistema deverá redirecionar o pacinte para a fila de atendimento
