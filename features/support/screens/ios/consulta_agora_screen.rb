# frozen_string_literal: true

# Classe Consulta online
class ShedulenowIOS
  attr_reader :btn_sheduled_ios, :btn_wait_queue_ios, :btn_out_queue_ios, :msg_position_ios, :mss_dialog_ios, :btn_confirm_ios

  def initialize
    @btn_sheduled_ios = EL_IOS['btn_sheduled_ios']
    @btn_wait_queue_ios = EL_IOS['btn_wait_queue_ios']
    @btn_out_queue_ios = EL_IOS['btn_out_queue_ios']
    @msg_position_ios = EL_IOS['msg_position_ios']
    @mss_dialog_ios = EL_IOS['mss_dialog_ios']
    @btn_confirm_ios = EL_IOS['btn_confirm_ios']
  end

  def access_sheduled
    find_element(xpath: @btn_sheduled_ios).click
  end

  def access_queue
    find_element(xpath: @btn_wait_queue_ios).click
  end

  def validate_queue
    find_element(xpath: @msg_position_ios)
  end
end
