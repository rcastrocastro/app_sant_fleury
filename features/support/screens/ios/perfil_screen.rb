# frozen_string_literal: true

# Classe Perfil
class ProfileIOS
  attr_reader :btn_my_data_ios, :btn_logout_ios, :btn_confirm_ios, :btn_canceled_ios,
              :btn_change_pass_ios, :btn_faq_ios, :btn_confirm_logout_ios

  def initialize
    @btn_my_data_ios = EL_IOS['btn_my_data_ios']
    @btn_logout_ios = EL_IOS['btn_logout_ios']
    @btn_confirm_ios = EL_IOS['btn_confirm_ios']
    @btn_canceled_ios = EL_IOS['btn_canceled_ios']
    @btn_change_pass_ios = EL_IOS['btn_change_pass_ios']
    @btn_view_terms_ios = EL_IOS['btn_view_terms_ios']
    @btn_faq_ios = EL_IOS['btn_faq_ios']
    @btn_confirm_logout_ios = EL_IOS['btn_confirm_logout_ios']
  end

  def access_faq
    find_element(xpath: @btn_faq_ios).click
  end

  def access_my_data_menu
    find_element(xpath: @btn_my_data_ios).click
  end

  def access_change_password
    find_element(xpath: @btn_change_pass_ios).click
  end

  def access_view_terms
    find_element(xpath: @btn_view_terms_ios).click
  end

  def logout
    find_element(xpath: @btn_logout_ios).click
    find_element(xpath: @btn_confirm_logout_ios).click
  end
end
