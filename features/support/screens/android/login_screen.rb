# frozen_string_literal: true

# Classe Login
class Login
  attr_reader :btn_skip, :user_email, :user_pass, :btn_entrar, :mss_dialog, :validate_page_login,
              :link_first_access

  def initialize
    @user_app = CONFIG['app']['user_app']
    @user_app_pass = CONFIG['app']['user_app_pass']
    @btn_skip = EL_ANDROID['btn_skip']
    @user_cpf = EL_ANDROID['user_cpf']
    @user_pass = EL_ANDROID['user_pass']
    @btn_entrar = EL_ANDROID['btn_entrar']
    @mss_dialog = EL_ANDROID['mss_dialog']
    @validate_page_login = EL_ANDROID['validate_page_login']
    @link_first_access = EL_ANDROID['link_first_access']
  end

  def sign_in
    find_element(id: @btn_skip).click
    find_element(id: @user_cpf).send_keys(@user_app)
    find_element(id: @user_pass).send_keys(@user_app_pass)
    find_element(id: @btn_entrar).click
  end

  def sign_in_failure(document, password)
    find_element(id: @btn_skip).click
    find_element(id: @user_cpf).send_keys(document)
    find_element(id: @user_pass).send_keys(password)
    find_element(id: @btn_entrar).click
  end

  def message_get
    find_element(id: @mss_dialog).text
  end

  def validate_element
    find_element(id: @validate_page_login)
  end

  def access_first_registration
    find_element(id: @btn_skip).click
    find_element(id: @link_first_access).click
  end
end
