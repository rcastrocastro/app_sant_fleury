Quando("realizar a troca da senha") do
  @screen.profile.access_change_password
  @screen.change_pass.change_pass_success
end

Então("o sistema deverá apresentar a mensagem de alteracao de senha") do |expect_message|
  expect(@screen.change_pass.validate_msg_change_pass_success).to eql expect_message
end

Quando("realizar a troca por um senha inválida") do
  @screen.profile.access_change_password
  @screen.change_pass.invalid_new_pass
end

Então("o sistema deverá apresentar a mensagem no campo da nova senha") do |expect_message|
  expect(@screen.change_pass.validade_msg_new_pass_invalid).to eql expect_message
end

Quando("preencher o campo de confirmacao com uma senha diferente") do
  @screen.profile.access_change_password
  @screen.change_pass.invalid_confirm_new_pass
end

Então("o sistema deverá apresentar a mensagem no campo de confirmacao") do |expect_message|
  expect(@screen.change_pass.validade_msg_confirm_new_pass_invalid).to eql expect_message
end