# frozen_string_literal: true

# Classe Visualizar FAQ
class FAQ
  attr_reader :validate_faq

  def initialize
    @validate_faq = EL_ANDROID['validate_faq']
  end

  def validate_page_faq
    find_element(id: @validate_faq).text
  end
end
