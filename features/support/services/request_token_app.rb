# frozen_string_literal: true

# Classe RequestTokenAPP
class RequestTokenAPP
  include HTTParty
  base_uri API['API_URI']
  headers API['API_HEADERS']
  default_timeout 120

  def initialize() end

  def user_token_app
    user_app = CONFIG['app']['user_app']
    pass_app = CONFIG['app']['user_app_pass']
    cat_token = { 'grant_type': 'password', 'input': { 'username': user_app.to_s, 'password': pass_app.to_s } }
    response = self.class.post('/oauth/access-token',
                               body: cat_token.to_json)

    token = response['access_token']

    token
  end
end
