Então("o sistema deverá apresentar {string} na área logada") do |expect_nameUser|
  expect(@screen.home.user_logged).to eql expect_nameUser
end

Quando("o usuário preencher os campos com {string} e {string}") do |cpf, pass|
  @screen.login.sign_in_failure(cpf, pass)
end

Então("o sistema apresentará a {string}") do |expect_message|
  expect(@screen.login.message_get).to eql expect_message
end

Quando("deslogar da aplicacao") do
  @screen.home.profile_access
  @screen.profile.logout
end

Então("verifico se o usuario foi deslogado") do
  expect(@screen.login.validate_element.displayed?).to be true
end
