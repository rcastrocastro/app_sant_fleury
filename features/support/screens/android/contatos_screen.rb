# frozen_string_literal: true

# Classe Alterar Contatos
class Contacts
  attr_reader :input_email, :clear_input_email, :input_phone, :clear_input_phone, :btn_add_secondary_phone,
              :input_secondary_phone, :clear_secondary_phone, :btn_refresh, :title_dialog

  def initialize
    @input_email = EL_ANDROID['input_email']
    @clear_input_email = EL_ANDROID['clear_input_email']
    @input_phone = EL_ANDROID['input_phone']
    @clear_input_phone = EL_ANDROID['clear_input_phone']
    @btn_add_secondary_phone = EL_ANDROID['btn_add_secondary_phone']
    @input_secondary_phone = EL_ANDROID['input_secondary_phone']
    @clear_secondary_phone = EL_ANDROID['clear_secondary_phone']
    @btn_refresh = EL_ANDROID['btn_refresh']
    @title_dialog = EL_ANDROID['title_dialog']
  end

  def add_secondary_phone(phone)
    find_element(id: @btn_add_secondary_phone).click
    find_element(id: @input_secondary_phone).send_keys(phone[:phone])
    find_element(id: @btn_refresh).click
  end

  def remove_secondary_phone
    find_element(id: @clear_secondary_phone).click
    find_element(id: @btn_refresh).click
  end

  def update_email(user)
    find_element(id: @clear_input_email).click
    find_element(id: @input_email).send_keys(user[:email])
    find_element(id: @btn_refresh).click
  end

  def cat_text
    find_element(id: @title_dialog).text
  end
end
