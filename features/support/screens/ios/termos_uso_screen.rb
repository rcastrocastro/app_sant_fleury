# frozen_string_literal: true

# Classe Visualizar Termos
class TermsIOS
  attr_reader :validate_terms_ios

  def initialize
    @validate_terms_ios = EL_IOS['validate_terms_ios']
  end

  def validate_page_terms
    find_element(xpath: @validate_terms_ios).text
  end
end
