Quando("atualizar o endereço dentro do menu Meus Dados") do
  @screen.profile.access_my_data_menu
  @screen.my_data.access_address
  @screen.address.update_address
end