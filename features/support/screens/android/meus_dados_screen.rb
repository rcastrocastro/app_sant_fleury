# frozen_string_literal: true

# Classe Menu Meus Dados
class Mydata
  attr_reader :btn_personal_data, :btn_contacts, :btn_address

  def initialize
    @btn_personal_data = EL_ANDROID['btn_personal_data']
    @btn_contacts = EL_ANDROID['btn_contacts']
    @btn_address = EL_ANDROID['btn_address']
  end

  def access_contacts
    find_element(id: @btn_contacts).click
  end

  def access_address
    find_element(id: @btn_address).click
  end
end
