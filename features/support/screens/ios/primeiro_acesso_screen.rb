# frozen_string_literal: true

# Classe Primeiro Acesso
class FirstaccessIOS
  attr_reader :close_keyboard_ios, :field_cpf_ios, :field_date_of_birth_ios, :radio_terms_ios, :btn_check_ios,
              :field_email_ios, :field_phone_ios, :btn_validate_email_ios, :field_register_pass_ios,
              :btn_validate_pass_ios, :msg_success_ios, :msg_failure_ios, :msg_cpf_invalid

  def initialize
    @field_cpf_ios = EL_IOS['field_cpf_ios']
    @set_input_cpf = CONFIG['register_first_access']['cpf']
    @close_keyboard_ios = EL_IOS['close_keyboard_ios']
    @set_input_cpf_invalid = CONFIG['register_first_access']['cpf_invalid']
    @field_date_of_birth_ios = EL_IOS['field_date_of_birth_ios']
    @input_date_of_birth = CONFIG['register_first_access']['date_of_birth']
    @set_input_date_invalid = CONFIG['register_first_access']['date_of_birth_invalid']
    @radio_terms_ios = EL_IOS['radio_terms_ios']
    @btn_check_ios = EL_IOS['btn_check_ios']
    @field_email_ios = EL_IOS['field_email_ios']
    @btn_validate_email_ios = EL_IOS['btn_validate_email_ios']
    @field_register_pass_ios = EL_IOS['field_register_pass_ios']
    @set_input_register_pass = CONFIG['register_first_access']['register_pass']
    @set_input_register_pass_invalid = CONFIG['register_first_access']['register_pass_invalid']
    @btn_validate_pass_ios = EL_IOS['btn_validate_pass_ios_ios']
    @msg_success_ios = EL_IOS['msg_success_ios']
    @msg_failure_ios = EL_IOS['msg_failure_ios']
    @msg_cpf_invalid = EL_IOS['msg_cpf_invalid']
  end

  def first_access_success
    find_element(xpath: @field_cpf_ios).send_keys(@set_input_cpf)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @field_date_of_birth_ios).send_keys(@input_date_of_birth)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @radio_terms_ios).click
    find_element(xpath: @btn_check_ios).click
    find_element(xpath: @btn_validate_email_ios).click
    find_element(xpath: field_register_pass_ios).send_keys(@set_input_register_pass)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @btn_show_pass_ios).click
    find_element(xpath: @btn_validate_pass_ios).click
  end

  def invalid_cpf_registration
    find_element(xpath: @field_cpf_ios).send_keys(@set_input_cpf_invalid)
    find_element(xpath: @close_keyboard_ios).click
  end

  def invalid_date_of_birth
    find_element(xpath: @field_cpf_ios).send_keys(@set_input_cpf)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @field_date_of_birth_ios).send_keys(@set_input_date_invalid)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @radio_terms_ios).click
    find_element(xpath: @btn_check_ios).click
  end

  def message_invalid_user
    find_element(xpath: @msg_failure_ios).text
  end

  def message_invalid_cpf
    find_element(xpath: @msg_cpf_invalid).text
  end

  def invalid_password
    find_element(xpath: @field_cpf_ios).send_keys(@set_input_cpf)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @field_date_of_birth_ios).send_keys(@input_date_of_birth)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @radio_terms_ios).click
    find_element(xpath: @btn_check_ios).click
    find_element(xpath: @btn_validate_email_ios).click
    find_element(xpath: field_register_pass_ios).send_keys(@set_input_register_pass_invalid)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @btn_show_pass_ios).click
    find_element(xpath: @btn_validate_pass_ios).click
  end

  def message_pass_invalid
    find_element(xpath: msg_textinput_error_ios).text
  end

  def message_success
    find_element(xpath: @msg_success_ios).text
  end
end
