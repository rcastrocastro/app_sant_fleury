Quando("acessar o faq") do
  @screen.profile.access_faq
end

Então("o sistema deverá redirecionar o paciente para o faq") do |expect_message|
  expect(@screen.faq.validate_page_faq).to eql expect_message
end
