# frozen_string_literal: true

# Classe Visualizar FAQ
class FAQIOS
  attr_reader :validate_faq_ios

  def initialize
    @validate_faq_ios = EL_IOS['validate_faq_ios']
  end

  def validate_page_faq
    find_element(xpath: @validate_faq_ios).text
  end
end
