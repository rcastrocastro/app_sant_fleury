# frozen_string_literal: true

# Classe HOME
class Home
  attr_reader :logged_user, :access_profile, :btn_consultations, :displayed_consultation, :btn_detail_consultation

  def initialize
    @logged_user = EL_ANDROID['logged_user']
    @access_profile = EL_ANDROID['access_profile']
    @btn_consultations = EL_ANDROID['btn_consultations']
    @displayed_consultation = EL_ANDROID['displayed_consultation']
    @btn_detail_consultation = EL_ANDROID['btn_detail_consultation']
  end

  def user_logged
    find_element(id: @logged_user).text
  end

  def profile_access
    find_element(id: @access_profile).click
  end

  def access_schedule
    find_element(id: @btn_consultations).click
  end

  def validate_schedule
    find_element(id: @displayed_consultation).displayed?
  end

  def select_query_detail
    find_elements(id: @btn_detail_consultation).sample.click
  end
end
