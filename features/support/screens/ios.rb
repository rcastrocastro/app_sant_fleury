# frozen_string_literal: true

require_relative './ios/alterar_senha_screen.rb'

class IOSScreens
  attr_accessor :address, :change_pass, :consultation, :contacts, :details,
                :faq, :first_access, :home, :login, :my_data, :profile, :request_token,
                :schedule, :schedule_now, :scheduling_doctor, :terms

  def initialize
    @address = AddressIOS.new
    @change_pass = ChangePassIOS.new
    @consultation = ConsultationIOS.new
    @contacts = ContactsIOS.new
    @details = ConsultationDetailsIOS.new
    @faq = FAQIOS.new
    @first_access = FirstaccessIOS.new
    @home = HomeIOS.new
    @login = LoginIOS.new
    @my_data = MydataIOS.new
    @profile = ProfileIOS.new
    @request_token = RequestTokenAPP.new
    @schedule = ScheduleIOS.new
    @schedule_now = ShedulenowIOS.new
    @scheduling_doctor = SchedulingDoctor.new
    @terms = TermsIOS.new
  end
end
