# frozen_string_literal: true

# Classe Detalhes da Consulta
class ConsultationDetailsIOS
  attr_reader :detail_consultation_ios, :btn_delete_ios, :btn_confirm_deletion_ios, :msg_delete_schedule_ios

  def initialize
    @detail_consultation_ios = EL_IOS['detail_consultation_ios']
    @btn_delete_ios = EL_IOS['btn_delete_ios']
    @btn_confirm_deletion_ios = EL_IOS['btn_confirm_deletion_ios']
    @msg_delete_schedule_ios = EL_IOS['msg_delete_schedule_ios']
  end

  def details
    find_element(xpath: @detail_consultation_ios).text
  end

  def delete_query
    find_element(xpath: @btn_delete_ios).click
  end

  def confirm_deletion
    find_element(xpath: @btn_confirm_deletion_ios).click
  end

  def message_dialog
    find_element(xpath: @msg_delete_schedule_ios).text
  end
end
