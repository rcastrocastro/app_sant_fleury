#language: pt

@smoke
Funcionalidade: Login
  Como um usuário
  Quero ter acesso ao sistema
  Para ter acesso as funcionalidades do aplicativo

  Cenário: Login com sucesso
    Quando o usuário informar os campos de login corretamente
    Então o sistema deverá apresentar "Olá, Joel Dias" na área logada
  
  Esquema do Cenario: Login com usuario e ou senha invalidos
    Quando o usuário preencher os campos com <user> e <password>
    Então o sistema apresentará a <mensagem>

    Exemplos:
      | user          | password   | mensagem                     |
      | "02308885858" | "123mudar" | "Usuário ou senha inválidos" |
  
  Cenário: Efetuar logoff do usuario
    Quando o usuário informar os campos de login corretamente
    E deslogar da aplicacao
    Então verifico se o usuario foi deslogado