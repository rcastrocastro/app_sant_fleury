# frozen_string_literal: true

require_relative './android/alterar_senha_screen.rb'

class DroidScreens
  attr_accessor :address, :change_pass, :consultation, :contacts, :details,
                :faq, :first_access, :home, :login, :my_data, :profile, :request_token,
                :schedule, :schedule_now, :scheduling_doctor, :terms

  def initialize
    @address = Address.new
    @change_pass = ChangePass.new
    @consultation = Consultation.new
    @contacts = Contacts.new
    @details = ConsultationDetails.new
    @faq = FAQ.new
    @first_access = Firstaccess.new
    @home = Home.new
    @login = Login.new
    @my_data = Mydata.new
    @profile = Profile.new
    @request_token = RequestTokenAPP.new
    @schedule = Schedule.new
    @schedule_now = Shedulenow.new
    @scheduling_doctor = SchedulingDoctor.new
    @terms = Terms.new
  end
end
