# frozen_string_literal: true

# Classe Escolher tipo da consulta
class Consultation
  attr_reader :btn_consult

  def initialize
    @btn_consult = EL_ANDROID['btn_consult']
  end

  def access_consult_now
    find_elements(id: @btn_consult)[0].click
  end

  def access_schedule_online
    find_elements(id: @btn_consult)[1].click
  end
end
