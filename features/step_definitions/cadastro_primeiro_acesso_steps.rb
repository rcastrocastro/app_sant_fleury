Dado("que o usuário esteja na tela de validacao") do
  @screen.login.access_first_registration
end

Quando("preencher todos os campos") do
  @screen.first_access.first_access_success
end

Então("o sistema deverá apresentar a mensagem de cadastro realizado com sucesso") do |expect_message|
  expect(@screen.first_access.message_success).to eql expect_message
end

Quando("preencher os preencher os campos com uma data de nascimento invalida") do
  @screen.first_access.invalid_date_of_birth
end

Então("o sistema deverá apresentar a mensagem de usuário não encontrado") do |expect_message|
  expect(@screen.first_access.message_invalid_user).to eql expect_message
end

Quando("preencher os preencher os campos com um cpf invalido") do
  @screen.first_access.invalid_cpf_registration
end

Então("o sistema deverá apresentar a mensagem de cpf invalido") do |expect_message|
  expect(@screen.first_access.message_invalid_cpf).to eql expect_message
end

Quando("preencher os preencher o campo de senha sem preencher os requisitos") do
  @screen.first_access.invalid_password
end

Então("o sistema deverá apresentar a mensagem de senha invalida") do |expect_message|
  expect(@screen.first_access.message_pass_invalid).to eql expect_message
end