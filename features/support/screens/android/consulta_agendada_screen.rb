# frozen_string_literal: true

# Classe Agendamento
class Schedule
  attr_reader :radio_first_consultation, :btn_consult, :calendar_hours, :btn_confirm_consultation,
              :btn_choose_doctor, :choose_doctor, :name_month_app, :advance_current_month,
              :btn_success_consultation, :field_name_search

  def initialize
    @radio_first_consultation = EL_ANDROID['radio_first_consultation']
    @btn_consult = EL_ANDROID['btn_consult']
    @calendar_hours = EL_ANDROID['calendar_hours']
    @btn_confirm_consultation = EL_ANDROID['btn_confirm_consultation']
    @btn_choose_doctor = EL_ANDROID['btn_choose_doctor']
    @choose_doctor = EL_ANDROID['choose_doctor']
    @name_month_app = EL_ANDROID['name_month_app']
    @advance_current_month = EL_ANDROID['advance_current_month']
    @btn_success_consultation = EL_ANDROID['btn_success_consultation']
    @field_name_search = EL_ANDROID['field_name_search']
  end

  def set_first_consultation
    find_elements(id: @radio_first_consultation)[0].click
  end

  def choose_option_shedule
    find_element(id: @btn_consult).click
  end

  def choose_option_doctor
    find_element(xpath: @btn_choose_doctor).click
  end

  def cat_month_date(date)
    month = Date.parse(date).strftime('%B')
    month_translated = validates_month(month)

    month_translated
  end

  def cat_month_app
    text_element = find_element(id: @name_month_app).text
    text_month_app = text_element.split.first.gsub(',', '')

    text_month_app
  end

  def validate_text_month(month_translated, text_month_app, date)
    month_translated != text_month_app ? change_month(date) : choose_the_date(date)
  end

  def change_month(date)
    find_element(id: @advance_current_month).click
    choose_the_date(date)
  end

  def choose_the_date(date)
    day = Date.parse(date).strftime('%d')
    @calendar_day = "//android.widget.TextView[@text=#{day}]"

    find_element(xpath: @calendar_day).click
    find_elements(id: @calendar_hours).sample.click

    find_element(id: @btn_confirm_consultation).click
    find_element(id: @btn_success_consultation).click
  end

  def choose_the_doctor(name)
    first_name = name.split.first.downcase
    last_name = name.split.last.downcase
    find_element(id: @field_name_search).send_keys(last_name)
    doctor = find_elements(id: @choose_doctor, text: first_name)
    doctor[0].click
  end
end
