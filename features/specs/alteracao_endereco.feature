#language: pt

@smoke @wip_ios
Funcionalidade: Alteração de endereço
  Como um usuário
  Quero poder atualizar meu endereço
  Para que possa estar correto minhas informações

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Atualizar Endereço do paciente
  Dado que o paciente acesse o seu perfil
  Quando atualizar o endereço dentro do menu Meus Dados
  Então o sistema deverá apresentar a mensagem
  """
  Dados atualizados com sucesso
  """
