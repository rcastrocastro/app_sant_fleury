Dado("que o paciente acesse o seu perfil") do
  @screen.home.profile_access
end

Quando("adicionar um novo contato dentro do menu Meus Dados") do |table|
  contact = table.hashes
  
  @screen.profile.access_my_data_menu
  @screen.my_data.access_contacts
  @screen.contacts.add_secondary_phone(contact[0])
end

Então("o sistema deverá apresentar a mensagem") do |expect_message|
  expect(@screen.contacts.cat_text).to eql expect_message
end

Quando("remover o contato secundario dentro do menu Meus Dados") do
  @screen.profile.access_my_data_menu
  @screen.my_data.access_contacts
  @screen.contacts.remove_secondary_phone
end

Quando("alterar o email cadastrado dentro do menu Meus Dados") do |table|
  user = table.hashes

  @screen.profile.access_my_data_menu
  @screen.my_data.access_contacts
  @screen.contacts.update_email(user[0])
end