module Helpers

  def validates_month(value)
    case value
    when 'January'
      value = 'Janeiro'
    when 'February'
      value = 'Fevereiro'
    when 'March'
      value = 'Março'
    when 'April'
    value = 'Abril'
    when 'May'
      value = 'Maio'
    when 'June'
    value = 'Junho'
    when 'July'
    value = 'Julho'
    when 'August'
    value = 'Agosto'
    when 'September'
    value = 'Setembro'
    when 'October'
    value = 'Outubro'
    when 'November'
    value = 'Novembro'
    when 'December'
    value = 'Dezembro'
    end
    value
  end

  def slide_slowly_up
    Appium::TouchAction.new.long_press(x: 553, y: 1111).move_to(x: 548, y: 461).release.perform
  end

  def slide_slowly_down
    Appium::TouchAction.new.swipe(start_x: 349, start_y: 614, end_x: 370, end_y: 1196, durattion: 600).perform
  end

end