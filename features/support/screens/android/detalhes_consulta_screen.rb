# frozen_string_literal: true

# Classe Detalhes da Consulta
class ConsultationDetails
  attr_reader :detail_consultation, :btn_delete, :btn_confirm_deletion, :mss_dialog

  def initialize
    @detail_consultation = EL_ANDROID['detail_consultation']
    @btn_delete = EL_ANDROID['btn_delete']
    @btn_confirm_deletion = EL_ANDROID['btn_confirm_deletion']
    @mss_dialog = EL_ANDROID['mss_dialog']
  end

  def details
    find_element(id: @detail_consultation).text
  end

  def delete_query
    find_element(id: @btn_delete).click
  end

  def confirm_deletion
    find_element(id: @btn_confirm_deletion).click
  end

  def message_dialog
    find_element(id: @mss_dialog).text
  end
end
