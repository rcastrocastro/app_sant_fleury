# frozen_string_literal: true

# Classe Visualizar Termos
class Terms
  attr_reader :validate_terms

  def initialize
    @validate_terms = EL_ANDROID['validate_terms']
  end

  def validate_page_terms
    find_element(xpath: @validate_terms).text
  end
end
