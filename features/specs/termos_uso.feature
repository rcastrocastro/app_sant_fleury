#language: pt

@smoke
Funcionalidade: Termos de uso
  Como um usuário
  Quero poder acessar os termos de uso
  Para que possa ter conhecimento das regras para uso do app

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Visualizar termos de uso
  Dado que o paciente acesse o seu perfil
  Quando acessar os termos
  Então o sistema deverá redirecionar o paciente para os termos
  """
  Termos de uso
  """