#language: pt

@smoke
Funcionalidade: Visualizar detalhe e excluir consulta
  Como um usuário
  Quero poder visualizar os detalhes e exluir a consulta
  Para que possa consulta o horário e/ou excluir a consulta agendada

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Visualizar detalhe da consulta agendada
  Quando selecionar a consulta para ver os detalhes
  Então o sistema deverá apresentar os detalhes

Cenário: Validar mensagem de exclusão da consulta agendada
  Quando o paciente clicar em excluir a consulta
  Então o sistema deverá apresentar a mensagem de exclusao
  """
  Você tem certeza que deseja excluir o seu agendamento?
  """

Cenário: Excluir consulta agendada
  Quando o paciente clicar em excluir a consulta
  E confirmar a exclusao
  Então o paciente deverá ser redirecionando para a home do app

