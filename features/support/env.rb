require 'appium_console'
require 'appium_lib'
require 'byebug'
require 'httparty'
require 'httparty/request'
require 'httparty/response/headers'

require_relative '../../features/support/helpers.rb'

include Helpers

CONFIG = YAML.load_file(File.join(Dir.pwd, 'features/support/config/Data/mass_data.yml'))
API = YAML.load_file(File.join(Dir.pwd, "features/support/config/environments/#{ENV["ENV_TYPE"]}.yml"))
EL_ANDROID = YAML.load_file(File.join(Dir.pwd, 'features/support/config/elements/android/elements_android.yml'))
EL_IOS = YAML.load_file(File.join(Dir.pwd, 'features/support/config/elements/ios/elements_ios.yml'))

DATA = if ENV['DEV']
  YAML.load_file('features/support/config/environments/dev.yml')
elsif ENV['HML']
  YAML.load_file('features/support/config/environments/hml.yml')
else
  YAML.load_file('features/support/config/environments/dev.yml')
end

@device = ENV['DEVICE']

case @device
when 'android'
  @device_caps = File.expand_path("config/caps/android/appium.txt", __dir__)
when 'ios'
  @device_caps = File.expand_path("config/caps/ios/appium.txt", __dir__)
end

@caps = Appium.load_appium_txt file: "#{@device_caps}", verbose: true
Appium::Driver.new(@caps, true)
Appium.promote_appium_methods Object
