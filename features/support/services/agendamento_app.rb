# frozen_string_literal: true

# Classe Agendamento Medico
class SchedulingDoctor
  include HTTParty
  base_uri API['API_URI']
  headers  API['API_HEADERS']
  default_timeout 120

  def initialize() end

  def converted_day_start
    date_today = Date.today.strftime('%Y-%m-%d')
    hour_today = Time.now.strftime('%H:%M:%S')
    start_date = date_today + 'T' + hour_today

    start_date
  end

  def converted_day_end
    date_next  = (Date.today + 30).strftime('%Y-%m-%d')
    hour_today = Time.now.strftime('%H:%M:%S')
    end_date = date_next + 'T' + hour_today

    end_date
  end

  def cat_available_days(token, start_day, end_day, appointment_type, patient_quantity)
    self.class.headers 'access_token' => token.to_s
    self.class.get("/gds-paciente/v1/api/calendars/available-days?&startAt=#{start_day}.000Z&endAt=#{end_day}.000Z&timezone=America/Sao_Paulo&scheduleType=online&appointmentType=#{appointment_type}&patientQuantity=#{patient_quantity}")
  end

  def convert_from_utc_days(list)
    timestamp = list.sample.to_i
    convert = Time.at(timestamp / 1000).utc
    utc = convert.strftime('%Y-%m-%dT12:00:00')
    p utc

    utc
  end

  def convert_from_utc_times(value)
    timestamp = value.sample.to_i
    convert = Time.at(timestamp / 1000).utc
    utc = convert.strftime('%Y-%m-%dT%H:%M:%S')
    p utc

    utc
  end

  def cat_available_times(token, date, appointment_type, patient_quantity)
    self.class.headers 'access_token' => token.to_s
    doctor_list = self.class.get("/gds-paciente/v1/api/calendars/available-times?scheduleType=online&day=#{date}&appointmentType=#{appointment_type}&patientQuantity=#{patient_quantity}&timezone=America/Sao_Paulo")
    doctor_random_choose = doctor_list.sample['doctor']

    doctor_random_choose
  end

  def cat_available_times_id_doctor(token, id_user, date, appointment_type, patient_quantity)
    self.class.headers 'access_token' => token.to_s
    result = self.class.get("/gds-paciente/v1/api/calendars/available-times/#{id_user}?scheduleType=online&day=#{date}&appointmentType=#{appointment_type}&patientQuantity=#{patient_quantity}&timezone=America/Sao_Paulo")
    result['times']
  end

  def make_appointment(token, date_time, id_user, appointment_type, patient_quantity)
    body = {}
    body[:userId]          = id_user.to_s unless id_user.to_s.nil?
    body[:startAt]         = date_time unless date_time.nil?
    body[:appointmentType] = appointment_type unless appointment_type.nil?
    body[:patientQuantity] = patient_quantity unless patient_quantity.nil?
    body[:scheduleType]    = 'online'
    self.class.headers 'access_token' => token.to_s
    self.class.post('/gds-paciente/v1/api/appointments/', body: body.to_json)
  end

  def convert_from_utc_to_timezone(time)
    time_convert = Time.parse(time)
    t_convert = time_convert - 108_00
    t_schedule = t_convert.strftime('%H:%M:%S')
    p t_schedule

    t_schedule
  end

  def cat_list_appointment(token)
    d_start = converted_day_start
    day_start = Date.parse(d_start).strftime('%Y-%m-%d')
    d_end = converted_day_end
    day_end = Date.parse(d_end).strftime('%Y-%m-%d')
    status = 'scheduled'
    self.class.headers 'access_token' => token.to_s
    result = self.class.get("/gds-paciente/v1/api/appointments/?startAt=#{day_start}&endAt=#{day_end}&status[]=#{status}")

    result
  end

  def cat_id_appointment(list)
    id_consult = list.last['_id']
    id_consult
  end

  def time_scheduling(list)
    time_consult = list.last['startAt']
    time_consult
  end

  def consult_id_doctor(list)
    id_doctor = list['userId']
    id_doctor
  end

  def cat_name_doctor(token, id_user)
    date = converted_day_start
    appointment_type = 'firstConsultation'
    patient_quantity = 1
    self.class.headers 'access_token' => token.to_s
    result = self.class.get("/gds-paciente/v1/api/calendars/available-times/#{id_user}?scheduleType=online&day=#{date}&appointmentType=#{appointment_type}&patientQuantity=#{patient_quantity}&timezone=America/Sao_Paulo")
    name_doctor = result['doctor']['name']
    name_doctor
  end

  def cancel_appointment(token, id)
    body = {}
    body[:status] = 'canceled'
    self.class.headers 'access_token' => token.to_s
    self.class.delete("/gds-paciente/v1/api/appointments/#{id}", body: body.to_json)
  end
end
