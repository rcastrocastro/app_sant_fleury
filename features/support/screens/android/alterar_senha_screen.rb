# frozen_string_literal: true

# Classe Alterar Senha
class ChangePass
  attr_reader :field_pass_current, :field_new_pass, :field_confirm_new_pass, :btn_update_pass,
              :btn_show_pass, :set_input_pass, :msg_pass_change, :msg_textinput_error,
              :set_input_invalid_pass

  def initialize
    @field_pass_current = EL_ANDROID['field_pass_current']
    @field_new_pass = EL_ANDROID['field_new_pass']
    @field_confirm_new_pass = EL_ANDROID['field_confirm_new_pass']
    @btn_show_pass = EL_ANDROID['btn_show_pass']
    @btn_update_pass = EL_ANDROID['btn_update_pass']
    @set_input_pass = CONFIG['register_first_access']['register_pass']
    @set_input_invalid_pass = CONFIG['register_first_access']['register_pass_invalid']
    @msg_pass_change = EL_ANDROID['msg_pass_change']
    @msg_textinput_error = EL_ANDROID['msg_textinput_error']
  end

  def change_pass_success
    s_pass = find_elements(id: @btn_show_pass)
    find_element(id: @field_pass_current).send_keys(@set_input_pass)
    s_pass[0].click
    find_element(id: @field_new_pass).send_keys(@set_input_pass)
    s_pass[1].click
    find_element(id: @field_confirm_new_pass).send_keys(@set_input_pass)
    s_pass[2].click
    find_element(id: @btn_update_pass).click
  end

  def validate_msg_change_pass_success
    find_element(id: @msg_pass_change).text
  end

  def invalid_new_pass
    s_pass = find_elements(id: @btn_show_pass)
    find_element(id: @field_pass_current).send_keys(@set_input_pass)
    s_pass[0].click
    find_element(id: @field_new_pass).send_keys(@set_input_invalid_pass)
  end

  def validade_msg_new_pass_invalid
    find_elements(id: @msg_textinput_error)[0].text
  end

  def invalid_confirm_new_pass
    s_pass = find_elements(id: @btn_show_pass)
    find_element(id: @field_pass_current).send_keys(@set_input_pass)
    s_pass[0].click
    find_element(id: @field_new_pass).send_keys(@set_input_pass)
    s_pass[1].click
    find_element(id: @field_confirm_new_pass).send_keys(@set_input_invalid_pass)
    s_pass[2].click
  end

  def validade_msg_confirm_new_pass_invalid
    find_elements(id: @msg_textinput_error)[1].text
  end
end
