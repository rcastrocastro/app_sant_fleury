[caps]
platformName = "Android"
deviceName = "VirtualDevice"
udid = "192.168.56.101:5555"
app = "./APP/app-dev-release.apk"
appPackage = "br.com.grupofleury.gds.pacientes.dev"
appActivity = "br.com.grupofleury.gds.pacientes.ui.activity.MainActivity"
automationName = "uiautomator2"