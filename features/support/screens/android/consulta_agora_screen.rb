# frozen_string_literal: true

# Classe Consulta online
class Shedulenow
  attr_reader :btn_sheduled, :btn_wait_queue, :btn_out_queue, :msg_position, :mss_dialog, :btn_confirm

  def initialize
    @btn_sheduled = EL_ANDROID['btn_sheduled']
    @btn_wait_queue = EL_ANDROID['btn_wait_queue']
    @btn_out_queue = EL_ANDROID['btn_out_queue']
    @msg_position = EL_ANDROID['msg_position']
    @mss_dialog = EL_ANDROID['mss_dialog']
    @btn_confirm = EL_ANDROID['btn_confirm']
  end

  def access_sheduled
    find_element(id: @btn_sheduled).click
  end

  def access_queue
    find_element(id: @btn_wait_queue).click
  end

  def validate_queue
    find_element(id: @msg_position)
  end
end
