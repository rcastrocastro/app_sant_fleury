# frozen_string_literal: true

# Classe Alterar Contatos
class ContactsIOS
  attr_reader :edit_field_input_contact_ios, :clear_input_email_ios, :input_phone_ios, :clear_input_phone_ios, :btn_add_secondary_phone_ios,
              :input_secondary_phone_ios, :clear_secondary_phone_ios, :btn_refresh_ios, :title_dialog_ios, :close_keyboard_ios

  def initialize
    @edit_field_input_contact_ios = EL_IOS['edit_field_input_contact_ios']
    @clear_input_email_ios = EL_IOS['clear_input_email_ios']
    @input_phone_ios = EL_IOS['input_phone_ios']
    @clear_input_phone_ios = EL_IOS['clear_input_phone_ios']
    @btn_add_secondary_phone_ios = EL_IOS['btn_add_secondary_phone_ios']
    @input_secondary_phone_ios = EL_IOS['input_secondary_phone_ios']
    @clear_secondary_phone_ios = EL_IOS['clear_secondary_phone_ios']
    @btn_refresh_ios = EL_IOS['btn_refresh_ios']
    @title_dialog_ios = EL_IOS['title_dialog_ios']
    @close_keyboard_ios = EL_IOS['close_keyboard_ios']
  end

  def add_secondary_phone(phone)
    find_element(xpath: @btn_add_secondary_phone_ios).click
    field = find_elements(xpath: @edit_field_input_contact_ios)
    field[2].click
    field[2].send_keys(phone[:phone])
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @btn_refresh_ios).click
  end

  def remove_secondary_phone
    find_element(xpath: @clear_secondary_phone_ios).click
    find_element(xpath: @btn_refresh_ios).click
  end

  def update_email(user)
    email = find_elements(xpath: @edit_field_input_contact_ios)
    email[0].clear
    email[0].send_keys(user[:email])
    find_element(xpath: @btn_refresh_ios).click
  end

  def cat_text
    find_element(xpath: @title_dialog_ios).text
  end
end
