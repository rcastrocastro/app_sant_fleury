# frozen_string_literal: true

# Classe Alterar Senha
class ChangePassIOS
  attr_reader :field_change_pass_ios, :btn_update_pass_ios, :set_input_pass_ios, :msg_pass_change_ios, :msg_textinput_error_ios,
              :set_input_invalid_pass_ios, :msg_pass_weak, :msg_pass_different

  def initialize
    @field_change_pass_ios = EL_IOS['field_change_pass_ios']
    @close_keyboard_ios = EL_IOS['close_keyboard_ios']
    @btn_update_pass_ios = EL_IOS['btn_update_pass_ios']
    @set_input_pass_ios = CONFIG['register_first_access']['register_pass']
    @set_input_invalid_pass_ios = CONFIG['register_first_access']['register_pass_invalid']
    @msg_pass_change_ios = EL_IOS['msg_pass_change_ios']
    @msg_pass_different = EL_IOS['msg_pass_different']
    @msg_pass_weak = EL_IOS['msg_pass_weak']
  end

  def change_pass_success
    field_pwd = find_elements(xpath: '//XCUIElementTypeSecureTextField')
    field_pwd[0].send_keys(@set_input_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
    field_pwd[1].send_keys(@set_input_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
    field_pwd[2].send_keys(@set_input_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
    find_element(xpath: @btn_update_pass_ios).click
  end

  def validate_msg_change_pass_success
    find_element(xpath: @msg_pass_change_ios).text
  end

  def invalid_new_pass
    field_pwd = find_elements(xpath: '//XCUIElementTypeSecureTextField')
    field_pwd[0].send_keys(@set_input_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
    field_pwd[1].send_keys(@set_input_invalid_pass_ios)
  end

  def validade_msg_new_pass_invalid
    find_element(xapth: @msg_pass_weak).text
  end

  def invalid_confirm_new_pass
    field_pwd = find_elements(xpath: '//XCUIElementTypeSecureTextField')
    field_pwd[0].send_keys(@set_input_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
    field_pwd[1].send_keys(@set_input_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
    field_pwd[2].send_keys(@set_input_invalid_pass_ios)
    find_element(xpath: @close_keyboard_ios).click
  end

  def validade_msg_confirm_new_pass_invalid
    find_element(xapth: @msg_pass_different).text
  end
end
