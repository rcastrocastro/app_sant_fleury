Quando("selecionar a consulta para ver os detalhes") do
  @screen.home.select_query_detail
end

Então("o sistema deverá apresentar os detalhes") do
  expect(@screen.details.details).to eql 'Consulta online'
end

Quando("o paciente clicar em excluir a consulta") do
  @screen.home.select_query_detail
  @screen.details.delete_query
end

Quando("confirmar a exclusao") do
  @screen.details.confirm_deletion
end

Então("o sistema deverá apresentar a mensagem de exclusao") do |expext_message|
  expect(@screen.details.message_dialog).to eql expext_message
end

Então("o paciente deverá ser redirecionando para a home do app") do
  sleep 0.5
  steps %{
    Então o sistema deverá apresentar "Olá, Joel Dias" na área logada
  }
end
