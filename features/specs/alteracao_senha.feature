#language: pt

@smoke
Funcionalidade: Alteração de senha
  Como um usuário
  Quero poder trocar minha senha
  Para que possa alterar os dados de acesso

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Alterar a senha com sucesso
  Dado que o paciente acesse o seu perfil
  Quando realizar a troca da senha
  Então o sistema deverá apresentar a mensagem de alteracao de senha
  """
  Sua senha foi redefinida com sucesso!
  """

Cenário: Cadastro de nova senha inválida
  Dado que o paciente acesse o seu perfil
  Quando realizar a troca por um senha inválida
  Então o sistema deverá apresentar a mensagem no campo da nova senha
  """
  Essa senha é fraquinha, vamos melhorar? :(
  """

Cenário: Preenchimento de confirmação de senha inválida
  Dado que o paciente acesse o seu perfil
  Quando preencher o campo de confirmacao com uma senha diferente
  Então o sistema deverá apresentar a mensagem no campo de confirmacao
  """
  As senhas são diferentes
  """