# frozen_string_literal: true

# Classe Perfil
class Profile
  attr_reader :btn_my_data, :btn_logout, :btn_confirm, :btn_canceled,
              :btn_change_pass, :btn_faq

  def initialize
    @btn_my_data = EL_ANDROID['btn_my_data']
    @btn_logout = EL_ANDROID['btn_logout']
    @btn_confirm = EL_ANDROID['btn_confirm']
    @btn_canceled = EL_ANDROID['btn_canceled']
    @btn_change_pass = EL_ANDROID['btn_change_pass']
    @btn_view_terms = EL_ANDROID['btn_view_terms']
    @btn_faq = EL_ANDROID['btn_faq']
  end

  def access_faq
    find_element(id: @btn_faq).click
  end

  def access_my_data_menu
    find_element(id: @btn_my_data).click
  end

  def access_change_password
    find_element(id: @btn_change_pass).click
  end

  def access_view_terms
    find_element(id: @btn_view_terms).click
  end

  def logout
    find_element(id: @btn_logout).click
    find_element(id: @btn_confirm).click
  end
end
