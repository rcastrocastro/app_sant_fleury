#language: pt

@smoke
Funcionalidade: Cadastro Primeiro Acesso
  Como um usuário
  Quero poder realizar o cadastro de primeiro acesso
  Para que possa ter acesso as funcionalidades do aplicativo

Cenário: Cadastro realizado com sucesso
  Dado que o usuário esteja na tela de validacao
  Quando preencher todos os campos
  Então o sistema deverá apresentar a mensagem de cadastro realizado com sucesso
  """
  Que tal aproveitar e já agendar a sua primeira consulta online?
  """

Cenário: Usuário com data de nascimento inválida
  Dado que o usuário esteja na tela de validacao
  Quando preencher os preencher os campos com uma data de nascimento invalida
  Então o sistema deverá apresentar a mensagem de usuário não encontrado
  """
  O seu usuário ainda não foi liberado. Converse com o RH da sua empresa e solicite a liberação para prosseguir.
  """

Cenário: Usuário com CPF inválido
  Dado que o usuário esteja na tela de validacao
  Quando preencher os preencher os campos com um cpf invalido
  Então o sistema deverá apresentar a mensagem de cpf invalido
  """
  CPF inválido
  """

Cenário: Senha não contempla requesitos de segurança
  Dado que o usuário esteja na tela de validacao
  Quando preencher os preencher o campo de senha sem preencher os requisitos
  Então o sistema deverá apresentar a mensagem de senha invalida
  """
  Essa senha é fraquinha, vamos melhorar? :(
  """
