#language: pt

@smoke @wip_ios
Funcionalidade: Me ajude
  Como um usuário
  Quero poder acessar o faq
  Para que possa ter tirar dúvidas

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Visualizar FAQ
  Dado que o paciente acesse o seu perfil
  Quando acessar o faq
  Então o sistema deverá redirecionar o paciente para o faq
  """
  Como podemos te ajudar ?
  """