# frozen_string_literal: true

# Classe Agendamento
class ScheduleIOS
  attr_reader :radio_first_consultation_ios, :btn_choose_calendar_ios, :calendar_hours_ios, :btn_confirm_consultation_ios,
              :btn_choose_doctor_ios, :choose_doctor_ios, :name_month_app_ios, :advance_current_month_ios,
              :btn_success_consultation_ios, :field_name_search_ios, :close_keyboard_ios, :hours_schedule_doctor_ios

  def initialize
    @radio_first_consultation_ios = EL_IOS['radio_first_consultation_ios']
    @btn_choose_calendar_ios = EL_IOS['btn_choose_calendar_ios']
    @btn_schedule_consult_ios = EL_IOS['btn_cobtn_schedule_consult_ios']
    @calendar_hours_ios = EL_IOS['calendar_hours_ios']
    @btn_confirm_consultation_ios = EL_IOS['btn_confirm_consultation_ios']
    @btn_choose_doctor_ios = EL_IOS['btn_choose_doctor_ios']
    @choose_doctor_ios = EL_IOS['choose_doctor_ios']
    @name_month_app_ios = EL_IOS['name_month_app_ios']
    @advance_current_month_ios = EL_IOS['advance_current_month_ios']
    @btn_success_consultation_ios = EL_IOS['btn_success_consultation_ios']
    @field_name_search_ios = EL_IOS['field_name_search_ios']
    @close_keyboard_ios = EL_IOS['close_keyboard_ios']
    @hours_schedule_doctor_ios = EL_IOS['hours_schedule_doctor_ios']
  end

  def set_first_consultation
    find_element(xpath: @radio_first_consultation_ios).click
  end

  def choose_option_shedule
    find_element(xpath: @btn_choose_calendar_ios).click
  end

  def choose_option_doctor
    find_element(xpath: @btn_choose_doctor_ios).click
  end

  def cat_month_date(date)
    month = Date.parse(date).strftime('%B')
    month_translated = validates_month(month)

    month_translated
  end

  def cat_month_app
    sleep 3
    text_element = find_elements(xpath: @name_month_app_ios)[1].text
    text_month_app = text_element.split.first.gsub(',', '')

    text_month_app
  end

  def validate_text_month(month_translated, text_month_app, date)
    month_translated != text_month_app ? change_month(date) : choose_the_date(date)
  end

  def change_month(date)
    find_element(xpath: @advance_current_month_ios).click
    choose_the_date(date)
  end

  def choose_the_date(date)
    day = Date.parse(date).strftime('%d')
    @calendar_day_ios = "//XCUIElementTypeStaticText[@value=#{day}]"

    find_element(xpath: @calendar_day_ios).click
    find_elements(xpath: @calendar_hours_ios).sample.click

    find_element(xpath: @btn_confirm_consultation_ios).click
    find_element(xpath: @btn_success_consultation_ios).click
  end

  def choose_the_doctor(name)
    first_name = name.split.first.downcase
    last_name = name.split.last.downcase
    sleep 3
    find_element(xpath: @field_name_search_ios).send_keys(last_name)
    find_element(xpath: @close_keyboard_ios).click
    doctor = find_elements(xpath: @choose_doctor_ios, text: first_name)
    doctor[0].click
  end

  def choose_date_doctor(date)
    day = Date.parse(date).strftime('%d')
    @calendar_day_ios = "//XCUIElementTypeStaticText[@value=#{day}]"

    find_element(xpath: @calendar_day_ios).click
    find_elements(xpath: @hours_schedule_doctor_ios).sample.click

    find_element(xpath: @btn_confirm_consultation_ios).click
    find_element(xpath: @btn_success_consultation_ios).click
  end

  def change_month_doctor(date)
    find_element(xpath: @advance_current_month_ios).click
    choose_date_doctor(date)
  end

  def validate_text_month_doctor(month_translated, text_month_app, date)
    month_translated != text_month_app ? change_month_doctor(date) : choose_date_doctor(date)
  end
end
