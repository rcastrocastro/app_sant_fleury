# frozen_string_literal: true

# Classe Menu Meus Dados
class MydataIOS
  attr_reader :btn_personal_data_ios, :btn_contacts_ios, :btn_address_ios

  def initialize
    @btn_personal_data_ios = EL_IOS['btn_personal_data_ios']
    @btn_contacts_ios = EL_IOS['btn_contacts_ios']
    @btn_address_ios = EL_IOS['btn_address_ios']
  end

  def access_contacts
    find_element(xpath: @btn_contacts_ios).click
  end

  def access_address
    find_element(xpath: @btn_address_ios).click
  end
end
