Dado("que o paciente selecione a opcao de agendar consulta") do 
  @screen.home.access_schedule
  @screen.consultation.access_schedule_online
  @screen.schedule.set_first_consultation
end

Quando("escolher a data com o horario da consulta") do
  token_app = @screen.request_token.user_token_app
  start_date = @screen.scheduling_doctor.converted_day_start
  end_date = @screen.scheduling_doctor.converted_day_end
  appointment_type = 'firstConsultation'
  patient_quantity = 1
  list_date = @screen.scheduling_doctor.cat_available_days(token_app, start_date, end_date, appointment_type, patient_quantity)
  utc_date = @screen.scheduling_doctor.convert_from_utc_days(list_date)
  doctor_choose = @screen.scheduling_doctor.cat_available_times(token_app, utc_date, appointment_type, patient_quantity)
  doctor_id = doctor_choose['id']
  doctor_name = doctor_choose['name']
  list_times = @screen.scheduling_doctor.cat_available_times_id_doctor(token_app, doctor_id, utc_date, appointment_type, patient_quantity)
  scheduling_time = @screen.scheduling_doctor.convert_from_utc_times(list_times)

  @screen.schedule.choose_option_shedule
  month_date_consultation = @screen.schedule.cat_month_date(scheduling_time)
  month_date_app = @screen.schedule.cat_month_app
  @screen.schedule.validate_text_month(month_date_consultation, month_date_app, scheduling_time)
end

Então("o sistema deverá apresentar na home a consulta agendada") do
  expect(@screen.home.validate_schedule).to eql true
end

Quando("escolher o medico") do
  token_app = @screen.request_token.user_token_app
  start_date = @screen.scheduling_doctor.converted_day_start
  end_date = @screen.scheduling_doctor.converted_day_end
  appointment_type = 'firstConsultation'
  patient_quantity = 1
  list_date = @screen.scheduling_doctor.cat_available_days(token_app, start_date, end_date, appointment_type, patient_quantity)
  utc_date = @screen.scheduling_doctor.convert_from_utc_days(list_date)
  doctor_choose = @screen.scheduling_doctor.cat_available_times(token_app, utc_date, appointment_type, patient_quantity)
  doctor_id = doctor_choose['id']
  doctor_name = doctor_choose['name']
  list_times = @screen.scheduling_doctor.cat_available_times_id_doctor(token_app, doctor_id, utc_date, appointment_type, patient_quantity)
  scheduling_time = @screen.scheduling_doctor.convert_from_utc_times(list_times)

  @screen.schedule.choose_option_doctor
  @screen.schedule.choose_the_doctor(doctor_name)

  month_date_consultation = @screen.schedule.cat_month_date(scheduling_time)
  month_date_app = @screen.schedule.cat_month_app
  if ENV['ENV_DEVICE_PREFIX'].eql?('android')
    @screen.schedule.validate_text_month(month_date_consultation, month_date_app, scheduling_time)
  else
    @screen.schedule.validate_text_month_doctor(month_date_consultation, month_date_app, scheduling_time)
  end
end