#language: pt

@smoke @wip_ios
Funcionalidade: Alteração de contato
  Como um usuário
  Quero poder atualizar meus contatos
  Para que possa estar correto minhas informações

Contexto: Paciente realizou o login
  Dado que o paciente esteja logado no app

Cenário: Adicionar novo contato
  Dado que o paciente acesse o seu perfil
  Quando adicionar um novo contato dentro do menu Meus Dados
    | phone       |
    | 11939374222 |
  Então o sistema deverá apresentar a mensagem
  """
  Dados atualizados com sucesso
  """

Cenário: Remover contato secundário
  Dado que o paciente acesse o seu perfil
  Quando remover o contato secundario dentro do menu Meus Dados
  Então o sistema deverá apresentar a mensagem
  """
  Dados atualizados com sucesso
  """

@a
Cenário: Alterar email cadastrado
  Dado que o paciente acesse o seu perfil
  Quando alterar o email cadastrado dentro do menu Meus Dados
    | email                                 |
    | raphael_antonio_vieira+45@hotmail.com |
  Então o sistema deverá apresentar a mensagem
  """
  Dados atualizados com sucesso
  """