require "report_builder"
require "date"

Before do
  driver.start_driver
  driver.manage.timeouts.implicit_wait = 60
  
  @screen = DroidScreens.new if ENV['ENV_DEVICE_PREFIX'].eql?('android')
  @screen = IOSScreens.new if ENV['ENV_DEVICE_PREFIX'].eql?('ios')

end

After do
  screenshot = driver.screenshot_as(:base64)
  embed(screenshot, "image/png", "Screenshot")

  driver.quit_driver
end

at_exit do
  @infos = {
    'device' => ENV['ENV_DEVICE_PREFIX'],
    'environment' => ENV['ENV_PREFIX'],
    'Data do Teste' => Time.now.strftime('%d/%B/%Y'),
    'Hora do Teste' => Time.now.strftime('%HH:%MM:%SS')
  }

  ReportBuilder.configure do |config|
    config.input_path = "log/report.json"
    config.report_path = "log/index"
    config.report_types = [:html]
    config.report_title = "GDS Pacientes [APP - SantéCorp]"
    config.additional_info = @infos
    config.color = "indigo"
  end
  ReportBuilder.build_report
end
